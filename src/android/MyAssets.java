//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.apache.cordova.plugin;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class MyAssets {
    protected static final String TAG = "CordovaPlugin"; //MyAssets.class.getSimpleName();
    public static final String ASSET_LIST_NAME = "assets.lst";
    public static final String SYNC_DIR = "sync";
    public static final String HASH_EXT = ".md5";
    private final AssetManager assetManager;
    private final File externalDir;
    private final File copyToDir;

    public MyAssets(Context var1) throws IOException {
        File var2 = var1.getExternalFilesDir((String)null);
        this.copyToDir = var1.getExternalFilesDir((String)null);
        if(null == var2) {
            throw new IOException("cannot get external files dir, external storage state is " + Environment.getExternalStorageState());
        } else {
            this.externalDir = new File(var2, "sync");
            this.assetManager = var1.getAssets();
        }
    }

    public MyAssets(Context var1, String var2) {
        this.externalDir = new File(var2);
        this.assetManager = var1.getAssets();
        this.copyToDir = var1.getExternalFilesDir((String)null);
    }

    public File getExternalDir() {
        return this.externalDir;
    }

    public Map<String, String> getItems() throws IOException {
        HashMap var1 = new HashMap();
        Iterator var2 = this.readLines(this.openAsset("assets.lst")).iterator();

        while(var2.hasNext()) {
            String var3 = (String)var2.next();
            InputStreamReader var4 = new InputStreamReader(this.openAsset(var3 + ".md5"));
            var1.put(var3, (new BufferedReader(var4)).readLine());
        }
        return var1;
    }

    public Map<String, String> getExternalItems() {
        try {
            HashMap var1 = new HashMap();
            File var2 = new File(this.externalDir, "assets.lst");
            Iterator var3 = this.readLines(new FileInputStream(var2)).iterator();

            while(var3.hasNext()) {
                String var4 = (String)var3.next();
                String[] var5 = var4.split(" ");
                var1.put(var5[0], var5[1]);
            }

            return var1;
        } catch (IOException var6) {
            return Collections.emptyMap();
        }
    }

    public Collection<String> getItemsToCopy(String var1) throws IOException {
        ArrayList var2 = new ArrayList();
        ArrayDeque var3 = new ArrayDeque();
        var3.offer(var1);

        while(!var3.isEmpty()) {
            var1 = (String)var3.poll();
            String[] var4 = this.assetManager.list(var1);
            String[] var5 = var4;
            int var6 = var4.length;

            for(int var7 = 0; var7 < var6; ++var7) {
                String var8 = var5[var7];
                var3.offer(var8);
            }

            if(var4.length == 0) {
                var2.add(var1);
            }
        }

        return var2;
    }

    private List<String> readLines(InputStream var1) throws IOException {
        ArrayList var2 = new ArrayList();
        BufferedReader var3 = new BufferedReader(new InputStreamReader(var1));

        String var4;
        while(null != (var4 = var3.readLine())) {
            var2.add(var4);
        }

        return var2;
    }

    private InputStream openAsset(String var1) throws IOException {
        return this.assetManager.open((new File(this.externalDir, var1)).getPath());
    }

    public void updateItemList(Map<String, String> var1) throws IOException {
        //File var2 = new File(this.externalDir, "assets.lst");
        File var2 = new File(this.copyToDir, "assets.lst");

        PrintWriter var3 = new PrintWriter(new FileOutputStream(var2));
        Iterator var4 = var1.entrySet().iterator();

        while(var4.hasNext()) {
            Entry var5 = (Entry)var4.next();
            var3.format("%s %s\n", new Object[]{var5.getKey(), var5.getValue()});
        }

        var3.close();
    }

    public File copy(String var1) throws IOException {
        InputStream var2 = this.openAsset(var1);
        //File var3 = new File(this.externalDir, var1);
        File var3 = new File(this.copyToDir, var1);
        var3.getParentFile().mkdirs();
        FileOutputStream var4 = new FileOutputStream(var3);
        byte[] var5 = new byte[1024];

        int var6;
        while((var6 = var2.read(var5)) != -1) {
            if(var6 == 0) {
                var6 = var2.read();
                if(var6 < 0) {
                    break;
                }

                var4.write(var6);
            } else {
                var4.write(var5, 0, var6);
            }
        }

        var4.close();
        return var3;
    }

    public File syncAssets() throws IOException {
        ArrayList var1 = new ArrayList();
        ArrayList var2 = new ArrayList();
        Map var3 = this.getItems();
        Map var4 = this.getExternalItems();
        Iterator var5 = var3.keySet().iterator();
        String var6;

        while(var5.hasNext()) {
            var6 = (String)var5.next();
            if(((String)var3.get(var6)).equals(var4.get(var6)) && (new File(this.externalDir, var6)).exists()) {
                Log.i(TAG, String.format("Skipping asset %s: checksums are equal", new Object[]{var6}));
            } else {
                var1.add(var6);
            }
        }
        var2.addAll(var4.keySet());
        var2.removeAll(var3.keySet());
        var5 = var1.iterator();

        File var7;
        while(var5.hasNext()) {
            var6 = (String)var5.next();
            var7 = this.copy(var6);
            Log.i(TAG, String.format("Copying asset %s to %s", new Object[]{var6, var7}));
        }

        var5 = var2.iterator();
        while(var5.hasNext()) {
            var6 = (String)var5.next();
            var7 = new File(this.externalDir, var6);
            var7.delete();
            Log.i(TAG, String.format("Removing asset %s", new Object[]{var7}));
        }
        this.updateItemList(var3);
        //return this.externalDir;
        return this.copyToDir;
    }
}
