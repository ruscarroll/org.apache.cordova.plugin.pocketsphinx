var exec = require('cordova/exec');
function PocketSphinx()
{
    //console.log("PocketSphinx.js: is created");
    //alert('pocketShpinx is created');
}

PocketSphinx.prototype.showToast = function(successCallback, errorCallback){
    exec(successCallback, errorCallback, "PocketSphinx", "showToast", []);
}
PocketSphinx.prototype.psCreate = function(successCallback, errorCallback){
    exec(successCallback, errorCallback, "PocketSphinx", "psCreate", []);
}
PocketSphinx.prototype.psStart = function(successCallback, errorCallback){
    exec(successCallback, errorCallback, "PocketSphinx", "psStart", []);
}
PocketSphinx.prototype.psStop = function(successCallback, errorCallback){
    exec(successCallback, errorCallback, "PocketSphinx", "psStop", []);
}
var pocketSphinx = new PocketSphinx();
module.exports = pocketSphinx;
