package org.apache.cordova.plugin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.widget.Toast.makeText;
import static edu.cmu.pocketsphinx.SpeechRecognizerSetup.defaultSetup;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;
import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import android.content.Context;

import java.io.InputStream;

import org.apache.cordova.*;


public class PocketSphinx extends CordovaPlugin implements RecognitionListener {
    protected static final String TAG = "CordovaPlugin"; //MyAssets.class.getSimpleName();
    /* Named searches allow to quickly reconfigure the decoder */
    private static final String KWS_SEARCH = "wakeup";
    private static final String FORECAST_SEARCH = "forecast";
    private static final String DIGITS_SEARCH = "digits";
    private static final String PHONE_SEARCH = "phones";
    private static final String MENU_SEARCH = "menu";

    /* Keyword we are looking for to activate menu */
    private static final String KEYPHRASE = "jarvy";

    private SpeechRecognizer recognizer;
    private HashMap<String, Integer> captions;

    // reuse keyword spotting callback to pass information back to JS
    private CallbackContext ksCallback;
    private PluginResult pluginResult;
    private boolean created = false;

    /**
     * Constructor.
     */
    public PocketSphinx() {}
    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }
    public boolean execute(final String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        final int duration = Toast.LENGTH_SHORT;
        boolean result = true;

        if (action.equals("showToast")) {
            try {
                // Shows a toast
                cordova.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast toast = Toast.makeText(cordova.getActivity().getApplicationContext(), action, duration);
                        toast.show();
                    }
                });
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, action));
            }
            catch (Exception e) {
                result = false;
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, e.getMessage()));
            }
        } else if (action.equals("psCreate")) {
            try {
                psCreate();
                //callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, action));
                pluginResult = new PluginResult(PluginResult.Status.OK, action);
                pluginResult.setKeepCallback(true);
                callbackContext.sendPluginResult(pluginResult);
                this.ksCallback = callbackContext;
            }
            catch (Exception e) {
                result = false;
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, e.getMessage()));
            }
        } else if (action.equals("psStart")) {
            try {
                switchSearch(KWS_SEARCH);
                pluginResult = new PluginResult(PluginResult.Status.OK, action);
                pluginResult.setKeepCallback(true);
                callbackContext.sendPluginResult(pluginResult);
                this.ksCallback = callbackContext;
                //callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, action));
                // save keyword spotting callback for later
                //this.ksCallback = callbackContext;
            }
            catch (Exception e) {
                result = false;
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, e.getMessage()));
            }
        } else if (action.equals("psStop")) {
            try {
                recognizer.stop();
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, action));
            }
            catch (Exception e) {
                result = false;
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, e.getMessage()));
            }
        } else {

        }

        return result;
    }

    public void psCreate() {
        if (!this.created) {
            new AsyncTask<Void, Void, Exception>() {
                @Override
                protected Exception doInBackground(Void... params) {
                    try {
                        Context theContext = cordova.getActivity().getApplicationContext();
                        MyAssets myAssets = new MyAssets(theContext, "www/sync");
                        File assetDir = myAssets.syncAssets();
                        setupRecognizer(assetDir);
                    } catch (IOException e) {
                        return e;
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Exception result) {
                    if (result != null) {
                        Toast toast = Toast.makeText(cordova.getActivity().getApplicationContext(), "Failed to init recognizer " + result, Toast.LENGTH_LONG);
                        toast.show();

                    } else {
                        switchSearch(KWS_SEARCH);
                    }
                }
            }.execute();
        }
        this.created = true;
    }

    /**
     * Check if an asset exists. This will fail if the asset has a size < 1 byte.
     * @param context
     * @param path
     * @return TRUE if the asset exists and FALSE otherwise
     */
    public static boolean assetExists(Context context, String path) {
        boolean bAssetOk = false;
        try {
            InputStream stream = context.getAssets().open(path);
            stream.close();
            bAssetOk = true;
        } catch (IOException e) {
            Toast toast = Toast.makeText(context, "assetExists failed: "+e.toString(),  Toast.LENGTH_LONG);
            toast.show();
        }
        return bAssetOk;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recognizer.cancel();
        recognizer.shutdown();
    }

    /**
     * In partial result we get quick updates about current hypothesis. In
     * keyword spotting mode we can react here, in other modes we need to wait
     * for final result in onResult.
     */
    //@Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;

        String text = hypothesis.getHypstr();
        if (text.equals(KEYPHRASE)) {
            // we found our KEYPHRASE
            //switchSearch(MENU_SEARCH)
            //this.ksCallback.sendPluginResult(new PluginResult(PluginResult.Status.OK, "keyword found"));
            // post back to JS
            recognizer.stop();
            pluginResult = new PluginResult(PluginResult.Status.OK, "keyword found");
            pluginResult.setKeepCallback(true);
            this.ksCallback.sendPluginResult(pluginResult);
            //switchSearch(KWS_SEARCH);
            //makeText(cordova.getActivity().getApplicationContext(), "onPartialResult: " + text, Toast.LENGTH_SHORT).show();
        }
        //else if (text.equals(DIGITS_SEARCH))
        //    switchSearch(DIGITS_SEARCH);
        //else if (text.equals(PHONE_SEARCH))
        //    switchSearch(PHONE_SEARCH);
        //else if (text.equals(FORECAST_SEARCH))
        //    switchSearch(FORECAST_SEARCH);
        else
        {
            Toast toast = Toast.makeText(cordova.getActivity().getApplicationContext(), "onPartialResult: " + text,  Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * This callback is called when we stop the recognizer.
     */
    //@Override
    public void onResult(Hypothesis hypothesis) {
        if (hypothesis != null) {
            // we found our KEYPHRASE
            String text = hypothesis.getHypstr();
            //makeText(cordova.getActivity().getApplicationContext(), "onResult: " + text, Toast.LENGTH_SHORT).show();
        }
    }

    //@Override
    public void onBeginningOfSpeech() {
    }

    /**
     * We stop recognizer here to get a final result
     */
    //@Override
    public void onEndOfSpeech() {
        if (!recognizer.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH);
    }

    private void switchSearch(String searchName) {
        recognizer.stop();

        // If we are not spotting, start listening with timeout (10000 ms or 10 seconds).
        //if (searchName.equals(KWS_SEARCH))
        //    recognizer.startListening(searchName);
        //else
        recognizer.startListening(searchName, 10000);
    }

    private void setupRecognizer(File assetsDir) throws IOException {
        // The recognizer can be configured to perform multiple searches
        // of different kind and switch between them
        recognizer = defaultSetup()
                .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                //.setDictionary(new File(assetsDir, "cmudict-en-us.dict"))
                .setDictionary(new File(assetsDir, "jarvy.dict"))

                        // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                //.setRawLogDir(assetsDir)

                // Threshold to tune for keyphrase to balance between false alarms and misses
                // 1e-2f = 0.01
                .setKeywordThreshold(1e-10f)
                //.setKeywordThreshold(1)

                        // Use context-independent phonetic search, context-dependent is too slow for mobile
                .setBoolean("-allphone_ci", true)

                .getRecognizer();
        recognizer.addListener(this);
        /** In your application you might not need to add all those searches.
         * They are added here for demonstration. You can leave just one.
         */

        // Create keyword-activation search.
        recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);

        // Create grammar-based search for selection between demos
        //File menuGrammar = new File(assetsDir, "menu.gram");
        //recognizer.addGrammarSearch(MENU_SEARCH, menuGrammar);

        // Create grammar-based search for digit recognition
        //File digitsGrammar = new File(assetsDir, "digits.gram");
        //recognizer.addGrammarSearch(DIGITS_SEARCH, digitsGrammar);

        // Create language model search
        //File languageModel = new File(assetsDir, "weather.dmp");
        //recognizer.addNgramSearch(FORECAST_SEARCH, languageModel);

        // Phonetic search
        //File phoneticModel = new File(assetsDir, "en-phone.dmp");
        //recognizer.addAllphoneSearch(PHONE_SEARCH, phoneticModel);
    }

    @Override
    public void onError(Exception error) {
        //recognizer.stop();
        pluginResult = new PluginResult(PluginResult.Status.ERROR, error.getMessage());
        pluginResult.setKeepCallback(true);
        this.ksCallback.sendPluginResult(pluginResult);
        //Toast toast = Toast.makeText(cordova.getActivity().getApplicationContext(), error.getMessage(),  Toast.LENGTH_SHORT);
        //toast.show();
    }

    @Override
    public void onTimeout() {
        switchSearch(KWS_SEARCH);
    }

}
